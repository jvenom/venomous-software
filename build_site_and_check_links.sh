#!/bin/bash

# Run the :test Rake task, passing in "test_site" as the directory name.
# This task will build the Jekyll site into test_site, and then run
# HTMLProofer to check for broken links in the generated HTML files.
bundle exec rake test[test_site]

# HTMLProofer will return a status code of 1 if it finds a failure.
# Capture this so our GitLab CI job won't fail.
if [ "$?" != 0 ]
then
    exit 0
fi
