---
permalink: /about/
title: "About"
---

I'm a software engineer at USAA. I mostly work in Python and Java on backend stuff (services, databases, etc), though I've dabbled in web development as well. I also do a fair amount of sysadmin-y things as part of my job, both on-prem and in AWS.

I deeply enjoy exploring new technologies and frameworks. Lately I've been focused on learning Elixir and functional programming. It's a big change from what I'm used to, but I find it refreshing. I'm also passionate about software engineering best practices, CI/CD, test automation, and developer tooling. I'm always looking to apply this passion for the benefit of the public in the form of open data efforts and civic hacking.

I'm married to the love of my life, Tiffany. We enjoy spending time at home watching Netflix and trying to stop our cats, Salem and Loki, from destroying everything in the house. We live in Houston, and for the three weeks out of the year when the weather's nice[^1] here, we like to spend time outside hiking along the bayous. We moved here after eight years in San Antonio, which contrary to popular belief is only slightly less humid than Houston.

You can find this blog's source code [here](https://gitlab.com/jvenom/venomous-software).

## Talks

I'm trying to get in the habit of source-controlling the slides for talks I give. I use Calvin Hendryx-Parker's [cookiecutter-revealjs](https://github.com/calvinhp/cookiecutter-revealjs/), which is a neat little tool that generates [reveal.js](https://revealjs.com/) slides from a single markdown file using a clever combination of [make](https://www.gnu.org/software/make/) and [pandoc](https://pandoc.org/). I'm a huge fan of markdown, by the way. So much so that I use it to create this blog :stuck_out_tongue_winking_eye:.

- **Geospatial Software Development**, September 17, 2019
  - San Antonio Developers 2019 Q3 meetup
  - [video](https://youtu.be/UIdHHJXwo6c) ~ [slides](https://joshvernon.github.io/geospatial-development/) ~ [source](https://github.com/joshvernon/geospatial-development/)
- **Lonestar Elixir 2020 Recap**, March 13, 2020
  - A tech talk I gave to my coworkers recapping the 2020 Lonestar Elixir conference. Much of the talk was also giving them an intro to Elixir.
  - [slides](https://joshvernon.github.io/lonestar-elixir-2020-recap) ~ [source](https://github.com/joshvernon/lonestar-elixir-2020-recap)

[^1]: You could argue that the weather in Houston is nice for much of the year. However 95 degrees and 80% humidity doesn't really make for an enjoyable hike.

## Certifications

<div data-iframe-width="150" data-iframe-height="270" data-share-badge-id="aacc8194-1121-4023-9e50-944a12a9753f" data-share-badge-host="https://www.credly.com"></div><script type="text/javascript" async src="https://cdn.credly.com/assets/utilities/embed.js"></script>
