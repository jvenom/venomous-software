# Venomous Software

Source code for the Venomous Software blog site.

The blog uses [Jekyll](https://jekyllrb.com/) with the [Minimal Mistakes](https://mmistakes.github.io/minimal-mistakes/) theme. It uses a mostly unmodified configuration copied from the [mm-github-pages-starter](https://github.com/mmistakes/mm-github-pages-starter) sample repo. Currently it's deployed using GitLab CI and hosted with GitLab Pages.

## Running locally

Ensure [bundler](https://bundler.io/) is installed. Then run the following:
```
bundle install
bundle exec jekyll serve
```

### Running the link checker

The link checker uses [HTMLProofer](https://github.com/gjtorikian/html-proofer) to check that all links on the site pages are working correctly. If you want to run the link checker locally you can run:

```
bundle exec rake test
```

This will build the site like you'd expect in the `_site` folder, then run `HTMLProofer` against it.

GitLab CI will also run the link checker on all feature branches, via a [shell script wrapper](./build_site_and_check_links.sh). It won't run on `master`. Link checker output will be included in the CI job logs, but the job won't fail if any link failures are encountered. This is because some of the links are intermittently unavailable or `HTMLProofer` consistently has issues hitting them.
