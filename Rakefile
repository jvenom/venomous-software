require 'html-proofer'

# A test task that takes in a directory name and runs HTMLProofer to
# check external links within the directory.
task :test, [:directory] do |t, args|
  # If no :directory argument is provided, use the Jekyll default
  # directory of _site.
  args.with_defaults(:directory => '_site')

  # Explicitly set the encoding to UTF-8 since by default it will be
  # US-ASCII in the Docker image used by the CI pipeline.
  # When run inside a container, nokogumbo will fail with an
  # InvalidByteSequenceError if the encoding isn't explicitly set here.
  Encoding.default_external = 'UTF-8'
  
  sh "bundle exec jekyll build -d #{args.directory}"
  options = {
    swap_urls: { %r{^/venomous-software/} => '/' },
    ignore_status_codes: [999]
  }
  HTMLProofer.check_directory(args.directory, options).run
end
