---
title: Building an Automated Jekyll Link Checker
date: 2022-09-13
tags:
  - jekyll
  - ruby
  - rake
  - ci
  - GitLab
---

As a web user and blog reader, nothing frustrates me more than broken links. As I start to write more blog posts, I want to ensure my readers don't face this frustration from _my_ blog. So I came up with the idea of writing a link-checking script to validate that all the links on this site are still valid. Here are my requirements for the link checker:
- It should check for broken links on all pages in the site.
- It should work with any build directory, not just the default `_site`.
- It should be easily run from both my local machine and a CI pipeline.

When I first started thinking about this, I thought about doing it from scratch in Python using a combination of [lxml](https://lxml.de/) and [requests](https://requests.readthedocs.io/en/latest/). When trying to solve a new problem it's natural to reach for the tools you're most comfortable with, and I'm far more comfortable with the Python ecosystem than I am with the Ruby one. But I'm really glad I didn't take that approach. I think I ended up with a better solution.

At the core of that solution is the [HTMLProofer](https://github.com/gjtorikian/html-proofer) Ruby library. It's feature-rich and relatively well-maintained. Besides links, it can also check images, scripts, and several other things. You can try out the various configuration options with the `htmlproofer` CLI script that's added when you install the gem. It also provides the `HTMLProofer` module that you can use directly in Ruby code. They even have a [sample configuration for Jekyll](https://github.com/gjtorikian/html-proofer#adjusting-for-a-baseurl) in their README. We'll build off that as we create our own solution.

First, add the html-proofer and rake gems to your Gemfile:

```ruby
gem "html-proofer"
gem "rake"
```

Then, install them with `bundle install`:
```
bundle install
```

The sample Jekyll configuration on the html-proofer GitHub page recommends writing your checker script as a rake task. We'll also take that approach. However we're going to extend the example by parameterizing the rake task so that it accepts a directory name as an argument and uses the Jekyll default of `_site` if none is provided:

```ruby
# Rakefile

require 'html-proofer'

task :test, [:directory] do |t, args|
  args.with_defaults(:directory => '_site')
  Encoding.default_external = 'UTF-8'
  sh "bundle exec jekyll build -d #{args.directory}"
  options = {
    swap_urls: { %r{^/YOUR-BASEURL-HERE/} => '/' },
    ignore_status_codes: [999]
  }
  HTMLProofer.check_directory(args.directory, options).run
end
```

The task is pretty straightforward. It takes in a directory name as an argument, and uses `_site` as the default if no argument is passed in. Then it runs `jekyll build` to build the site artifacts into that directory. We set a hash of options that HTMLProofer will accept, then we run the `HTMLProofer.check_directory` method with the directory name and the options hash as arguments.

The `swap_urls` option is something you'll need if have the `baseurl` set in your blog's Jekyll config. Substitute your `baseurl` value for the text that says `YOUR-BASEURL-HERE`. The `ignore_status_codes` option tells HTMLProofer to ignore certain HTTP status codes. You can adjust the list to your needs or omit that option altogether.

The only other line that needs a little extra explanation is this one:

```ruby
Encoding.default_external = 'UTF-8'
```

If you're running this task with a standard Ruby installation on your laptop, the encoding will likely already be set to `UTF-8` by default, so why do we explicitly set it here? When I ran this task for the first time in a GitLab CI pipeline, I was seeing an `InvalidByteSequenceError` when calling `HTMLProofer`. It turns out the encoding was set to `US-ASCII` by default in the Docker container the pipeline was using. I got around this error by explicitly setting the encoding to `UTF-8` in the rake task.

That's pretty much the meat of the checker. You can run it locally with the following:

```
bundle exec rake test
```

The rake task will build your site into the `_site` directory as expected, then run the checker on it and output any failures that it finds for broken links.

When I started running the checker, I noticed that there were a few links the checker consistently reported a problem with, even though I was able to hit them just fine in the browser. Other links seemed to be intermittently unavailable. This posed a problem for my GitLab CI pipeline, since HTMLProofer will return an exit code of "1" even if just one check fails, and the GitLab CI job will fail if it receives a non-zero return code. So I created a simple shell script wrapper to capture non-zero return codes and return a zero instead:

```bash
#!/bin/bash
# build_site_and_check_links.sh

bundle exec rake test[test_site]

if [ "$?" != 0 ]
then
    exit 0
fi
```

The rake task is invoked exactly the same as before, except this time we pass in a directory name of `test_site`. We can then call this shell script in a GitLab CI job, like so:

```yaml
# Partial .gitlab-ci.yml

# ...Other `before_script`s and jobs are defined here...

test-pages-build:
  stage: test
  script:
    - ./build_site_and_check_links.sh
  artifacts:
    paths:
      - test_site
    expire_in: 4 hrs
  except:
    - master
```

Each time we push to a feature branch, this job will run and check for broken links, but it won't fail if it finds any. The broken links will be printed to the CI job log, so we can take a look at them and fix them if needed.

Here's what that output looks like:

```
 $ ./build_site_and_check_links.sh
bundle exec jekyll build -d test_site
...Jekyll build output here...
Running 3 checks (Images, Scripts, Links) in ["test_site"] on *.html files...
Checking 85 external links
Checking 73 internal links
Ran on 18 files!
For the Links check, the following failures were found:
* At test_site/blog/home-buyer-beware/index.html:428:
  http://www.tax-rates.org/texas/property-tax is not an HTTPS link
For the Links > External check, the following failures were found:
* At test_site/blog/gitlab-staticman/index.html:455:
  External link https://gitlab.com/jvenom/venomous-software/-/blob/master/_config.yml#L33-42 failed: https://gitlab.com/jvenom/venomous-software/-/blob/master/_config.yml exists, but the hash 'L33-42' does not (status code 200)
* At test_site/blog/mainframe-musings/index.html:320:
  External link https://www.amazon.com/Pragmatic-Programmer-journey-mastery-Anniversary/dp/0135957052 failed (status code 503)
For the Links > Internal check, the following failures were found:
* At test_site/blog/gitlab-staticman/index.html:435:
  internally linking to /blog/deploying-jekyll-to-gitlab-pages/#comment1; the file exists, but the hash 'comment1' does not
HTML-Proofer found 4 failures!
```

That's it! Hopefully this post has shown you how easy it is to set up link-checking automation for your site, especially when you have a robust tool like `HTMLProofer` at your disposal.
