---
title: "Buyer Beware: Things to Consider Before Buying a Home"
tags:
  - personal finance
---

Buying a home is one of the most important and stressful decisions you'll ever make. As a recent home buyer I'm painfully aware of this fact. In this post I'm going to talk about some of the common mistakes people make when buying a home, and detail some of the expenses you'll want to consider once you decide you're ready, including actual dollar amounts that I spent on some of them.

Take my advice with a grain of salt; I'm not a financial professional, and there are many amateurs out there that are probably more knowledgeable than I am. If you want to tell me how wrong I am, feel free to leave a comment at the bottom of the post!

## Mistakes related to attitude and perception

Buying a home can be an emotional process. Sometimes these emotions can get the best of you and cause you to make rash decisions. Being aware of these common misconceptions will help you guard against them.

### Feeling pressured to buy a home when you're not financially ready

In America today you can buy almost anything on credit. Unless you have large amounts of cash or other assets, your home is probably going to be one of those things. It'll likely make up the biggest portion of your wealth, and your mortgage will likely be the largest portion of your debt.

Before buying a home you'll want to take a long, honest look at your own finances to determine if you're actually ready for this huge financial step. (**_You're probably not_.**) Do you have a large unpaid balance of student loans or auto loans? Do you have high-interest credit card debt? Do you find yourself struggling to pay your bills every month? Are you planning on a major life change in the next few years that'll be a big financial commitment, like having a child? Are you saving and investing, and do you have an emergency fund? These are the types of questions you'll need to ask yourself before you commit to the home buying process.

### Buying a home because "it's a great time to buy"

As soon as you mention that you're even thinking about buying a home, someone in your social circle is going to tell you what a great time it is to buy, even when [it isn't](https://home.com/bad-time-to-buy-a-house-worse-to-rent/). Don't listen to these people. Whether it's a "great time to buy" or not should be determined subjectively, by you, based on your own financial situation, not some random person's (likely ill-informed) perception of broader trends in the housing market.

### Viewing your potential home as an investment vehicle

Some people want to buy a home because they assume that its value will inevitably go up. This is not guaranteed. Real estate is like any other asset; its value can go up or down over time, usually due to circumstances you have little or no control over.

Futhermore, real estate is extremely [illiquid](https://www.investopedia.com/terms/l/liquidity.asp), meaning that it's difficult to convert to other types of assets. Think about it like this. If you were strapped for cash, which would be easier and would get you the cash in your pocket faster? Selling a bunch of stock shares, or selling your house?

I'm not saying it's impossible to make money off of real estate investments. It's absolutely possible, and many people do. However you shouldn't look at your _primary home_ this way. Buy a home because you'll want to live there for a long time, not because you want to sell it in a few years to (hopefully) make a profit.

### Not understanding your own ignorance and your own disadvantaged position in the home buying process

When you're a first time home buyer, you need to understand that literally everyone else participating in the process will have more experience and information than you do - the realtors, bankers, contractors, inspectors, and even the seller, who has likely bought a home themselves at least once before. Everyone is trying to make money, and sometimes that desire can supersede the impulse to provide you with accurate information so that you can make a responsible decision. If you're lucky you'll choose a realtor that can help you navigate this process effectively, hopefully reducing some of its complexity and filling in your information gaps. I wouldn't count on it, though.

A related point: your mortgage lender will likely approve you for a much higher loan than you can actually afford. Take their approval amount, and chop a hundred or two thousand dollars off that to get the amount you can actually reasonably afford.

## Home buying and home owning expenses

Now that I've covered some of the mistakes you can make when starting your home buying journey, I'm going to cover some expenses that you can expect to encounter when you're buying your home and after you move in.

### My numbers

When I wrote the first version of this blog post, I didn't give specific amounts for all of these expenses. I thought it'd be more useful and relevant to most readers to have specific numbers listed for reference. But to start off with here are the basic stats for my house:

- **House price:** $254,900
- **Down payment:**: $40,000
- **Location:** Houston, TX suburbs

Now I'll start listing some of the expenses you can expect to pay when you buy a home, starting with the closing costs.

### Closing costs

Buying a home isn't as simple as forking over a down payment and calling it day. Beyond your down payment, there are going to be other fees and expenses you'll need to be pay either before or at your closing. These include:

#### Earnest money deposit

This is a portion of your down payment due up front (before closing) to assure the seller that you're serious about buying the house. After you sign your purchase contract, you'll likely have only a small amount of time to put down the earnest money deposit, which will probably be several thousand dollars (it's usually a percentage of your down payment).

**What I paid:** $5000

#### Mortgage "points" and fees

"Points" are an extra amount you can choose pay at closing to reduce your interest rate. Each lender will have different points schemes, and it's not something that they usually publicize in a transparent or consistent way. Again, they're taking advantage of the information gap by keeping you in the dark.

**What I paid:** $2551.58 + $995 origination fee

#### Title fees

A real estate transaction is a complicated affair, so you'll usually hire a title company (or one will be chosen for you) to orchestrate the legal intricacies and manage all the paperwork and communication among the different parties. Like everyone else in the process, they don't perform this service for free. You can count on several hundreds or thousands of dollars in fees for these various activities. If you're lucky the seller might pay a portion of them, but more likely than not it'll mostly be paid by you.

**What I paid:** $2033.74

#### Initial escrow deposits

If you're paying your property taxes and homeowners insurance premiums as part of your mortgage payment (i.e. paying into an "escrow account"), you'll need to pay a certain percentage of these up front to make an initial deposit into your escrow account. Like all the other items above, this amount will vary based on the value of your home, where you live, and the mortgage lender you choose.

**What I paid:**
- Homeowners insurance premium (15 months worth): $2550
- Property taxes (7 months worth): $3975.30

#### Inspection fees

You'll need to pay for an inspector. Honestly this will be one of the cheapest things you pay for (at least compared to everything above), but it'll be the most valuable for you, because it gives you more information about the house (thus closing the information gap a little bit). Make sure you hire a good inspector; your realtor should be able to give you a recommendation.

You might also need to pay for other types of experts or contractors based on the results of your inspection. For example, my inspector recommended that a structural engineer come out and assess the house; I had to pay a separate fee for that as well.

**What I paid:** $450 (inspection) + $250 (structural engineer)

### Moving in

So you've closed on the house. Congratulations! Unfortunately, the financial pain doesn't stop after closing day. As you're preparing to move in, you're going to be hit with even more expenses.

#### Appliances

Up until now I've only lived in apartments, and I've been lucky enough to have all major appliances like fridges, washers, and dryers included at each one. This was great at the time, but it was a little daunting realizing I'd have to buy all these things for the new house. Luckily, my parents were gracious enough to step in and help me with these.

**What my parents paid**: $4293.50 for fridge, washer, dryer, hookup materials, delivery, and installation

#### Furniture

This is an expense that's often overlooked. You're likely buying a house because you want more space. Well, you have to fill that space with furniture. My wife and I moved from a 780 square foot apartment into a 2500 square foot house, so we had lots of things to buy. So much so that as of this writing, four and a half months after our closing and three months after moving in, we still don't have everything. And this was even with my parents stepping in to help, again. Here's some of the major furniture pieces you might need:

- beds for master and guest bedrooms
- dressers
- couches / armchairs
- TVs and TV stands
- dining room table(s) and chairs
- outdoor furniture
- desks and other office furniture

Again, I got lucky here as well. My friends were nice enough to give us their old king-size bed for one of the guest bedrooms and our dining room table.

**What I paid:** $2047.01, and counting

#### Moving expenses

If your new house is in a different city from where you currently live, you might need to pay movers. Of course it's possible to do a DIY move, but I refuse to do that since moving is one of things I hate most in this world. Maybe it doesn't bother you as much, or maybe you have family and friends willing to help you out. The point is that _someone_ is going to pay the cost of moving, either in cash or time, sweat, and tears. Even if you're moving within the same city, you can expect to shell out a few hundred dollars on movers.

**What I paid:** $1717.50, for a 192 mile move

### Ongoing expenses

Now that you're all moved in, there are no more expenses, right? Wrong, wrong, wrong. Home ownership is one of the biggest financial drains you'll ever endure, one that'll now be with you for the rest of your life.

#### Property taxes

I live in Texas, which has some of the [highest property tax rates in the U.S.](http://www.tax-rates.org/texas/property-tax) We have no state or local income taxes here, so property taxes are pretty much the main method local jurisdictions such as counties, school districts, and hospital districts use to bring in revenue.

I chose to pay my property taxes from an escrow account, which is nice because the mortgage company will pay them every year and I don't have to worry about it. However it also means that my monthly mortgage payment is significantly higher than what I'd pay if I were only paying principal and interest.

**What I'm paying:** $567.90 / month added to my mortgage payment

#### Homeowners insurance

When I lived in an apartment, I was required to hold renters insurance, and the premium cost about 120 dollars a year. Homeowners insurance will be much, much more than that (probably at least four figures). Like property taxes, you can have your mortgage company pay the annual premium from an escrow account, but again that'll make your mortgage payment higher.

When disaster strikes - such as a fire, water leak, or storm damage - and you have to file a claim, you'll also have to pay the deductible specified in your policy. This might be four figures or more. Also, different claim types such as wind or hail damage might have higher deductibles than what you'd normally pay for other claims.

**What I'm paying:** $170 / month added to my mortgage payment

#### Home maintenance

Inevitably something in your home is going to break, and you'll likely need to call someone to repair it, or buy a replacement. Most of these repairs will usually run you a few hundred dollars, however some of them can be much more costly, like:

- replacing a roof
- replacing a major appliance like a dishwasher or water heater
- foundation issues
- tree roots in water or sewer lines

Some of these things have a defined lifespan - for example, you know you're probably going to have to replace your roof after about 15 to 20 years. Other ones are unpredictable, like water clogs or leaks. The key is to keep these types of expenses in mind when you're saving money in your emergency fund, especially after you've probably just depleted it paying for your closing costs and your new appliances.

**What I've payed so far:** Several hundred dollars at Lowe's + ~$1200 for water damage and roof repair, and counting

## Conclusion: what to do right now instead of buying a home

If you've read this entire post and you no longer want to buy a home, good. That was the point. Hopefully you've got a better grasp of the types of expenses you can expect to encounter when buying a home. But you might be asking yourself what you _can_ do in order to better prepare for home buying. The answer is pretty simple, if unsatisfying: _wait_, and _save_.

Consider yourself lucky that home buying is even on your financial horizon. Many people can't say the same, either because of their personal financial situation or because homes where they live are vastly unaffordable for all but the richest people. I've been lucky enough to have gotten past help from my parents and grandparents that allowed me to jump-start my savings. The best thing you can do if you want to buy a home is to wait and continue saving. **If you think you're financially ready to buy a home right now, you're probably not.**

Make saving for a home part of your long-term savings and investment strategy. Instead of paying a mortgage payment every month, you can deposit extra money into a high-interest savings account or a brokerage account. Think of it like making a mini mortgage payment, but instead of a bank making money off your interest, the interest and principal are yours. Once you're finally ready to take the plunge and buy a home, you'll be able to put down a larger down payment, which will mean lower interest rates on your mortgage, reduced or waived [PMI](https://en.wikipedia.org/wiki/Lenders_mortgage_insurance), and a larger savings buffer for the inevitable renovations and repairs.

If you truly want to buy a home, make sure it's for the right reasons, and make sure you understand the huge financial commitment involved in this decision.
