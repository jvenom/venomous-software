---
title: Renaming files with Python's reduce() and pathlib
tags:
  - python
  - functional programming
  - music
---

This blog post was inspired by a [README](https://gitlab.com/jvenom/personal-stuff/-/tree/master/rename-music) I made for one of my personal utility scripts. That was basically a blog post already, so I thought I'd finish the job and publish it here.

## The problem

I have a bunch of tracks that I ripped from a CD several years ago. The CD was a special release from a local (now defunct) radio station, so it's not available from any of the main music streaming services I use. The tracks are stored as `*.ogg` files in two directories on my personal PC's hard drive. I wanted to copy all these tracks to an external hard drive.

For some reason, when I ripped the tracks I named them all with the pattern `track number|artist|title.ogg`, where `artist` and `title` have spaces, parentheses, colons, apostrophes, and other nasty special characters. This was causing all kinds of problems for me when I was trying to copy the files around on my Linux machine. I wanted to rename them all to something more sensible, while still retaining the same track_number/artist/title structure in the name.

I decided to write a Python script to rename the files by replacing the special characters in the file name. This is my jam - [over-engineering](https://github.com/joshvernon/teacher-expense-tracker) a solution to a [trivial problem](https://github.com/joshvernon/payday-leap-year-calculator) that could easily be solved without code.

Take a look at some of these disgusting file names, output from `ls`. 2012 me deserves a vigorous slap in the face. Yuck :nauseated_face:.

```
'10|Fritz & The Tantrums|Don'\''t Gotta Work It Out.ogg'
'13|Kris Kristofferson|The Pilgrim: Chapter 33.ogg'
'9|Michael Kiwanuka|I'\''m Getting Ready.ogg'
```

## First attempt - the naïve approach

Ultimately what I wanted to do was to run `string.replace()` multiple times on the same string, using patterns that I specified. Something like this:

```python
for track in folder:
    new_name = track.replace('|', '_')
    new_name = new_name.replace(' ', '-')
    new_name = new_name.replace("'", "")
    # And so on
```

I wanted to externalize all the patterns somewhere outside the `for` loop where they'd be easier to change, and apply all the replacements at once.

## The data structure - a tuple of 2-tuples

I chose to store the patterns in a tuple of 2-tuples, where the first element in each 2-tuple is the special character, and the second element is what I want to replace it with.

```python
replace_patterns = (
    ('|', '_'),
    (' ', '-'),
    ("'", ""),
    (':', ''),
    ('(', ''),
    (')', ''),
    ('&', 'and'),
)
```

## reduce()

I thought, "This seems like a problem that could be solved with [reduce()](https://docs.python.org/3/library/functools.html#functools.reduce)." Eventually I found [this Stack Overflow answer](https://stackoverflow.com/a/9479972) that captured the syntax of what I was trying to do.

Here's what the renaming logic looks like now:

```python
for track in folder:
    new_name = functools.reduce(
        lambda string, patterns: string.replace(*patterns),
        replace_patterns,
        track
    )
```

To me, this seems much cleaner. The patterns themselves are separated from the replacement logic, and the code is easier to read.

## pathlib

This exercise was also an opportunity for me to play around more with Python 3's [pathlib](https://docs.python.org/3/library/pathlib.html) module. I find working with `Path`s much cleaner than Python 2's `os.path`, `os.*`, and `shutil` shenanigans. Using `pathlib` also makes it easier to write portable code that can run on multiple platforms. You can see some of `pathlib`'s features in the example code below.

## Complete script

Here's the full [script](https://gitlab.com/jvenom/personal-stuff/-/blob/master/rename-music/rename_tracks.py) I wrote. It iterates over the all the tracks and renames them to something less egregious.

```python
# rename_tracks.py

from functools import reduce
from pathlib import Path

REPLACE_PATTERNS = (
    ('|', '_'),
    (' ', '-'),
    ("'", ""),
    (':', ''),
    ('(', ''),
    (')', ''),
    ('&', 'and'),
)

def main():
    music_root = Path('~/Music/kgsr_broadcast_20')
    for child in music_root.expanduser().iterdir():
        print(f'Processing directory: {child}')
        for track in child.iterdir():
            # Path.name returns the file name without the path.
            current_name = track.name
            new_name = reduce(
                lambda string, patterns: string.replace(*patterns),
                REPLACE_PATTERNS,
                current_name
            )
            new_path = music_root / child / new_name
            track.rename(new_path)
            print(f'Renamed {track} to {new_path}')

if __name__ == '__main__':
    main()
```

## Results

Let's see how our example songs got renamed. Again, here's what the old names were:

```
'10|Fritz & The Tantrums|Don'\''t Gotta Work It Out.ogg'
'13|Kris Kristofferson|The Pilgrim: Chapter 33.ogg'
'9|Michael Kiwanuka|I'\''m Getting Ready.ogg'
```

Ugh. It makes my eyes hurt just looking at it. Let's look at the output of the script to see what they were renamed to:

```
Renamed ~/Music/kgsr_broadcast_20/disc1/10|Fritz & The Tantrums|Don't Gotta Work It Out.ogg to ~/Music/kgsr_broadcast_20/disc1
10_Fritz-and-The-Tantrums_Dont-Gotta-Work-It-Out.ogg
Renamed ~/Music/kgsr_broadcast_20/disc2/13|Kris Kristofferson|The Pilgrim: Chapter 33.ogg to ~/Music/kgsr_broadcast_20/disc2
13_Kris-Kristofferson_The-Pilgrim-Chapter-33.ogg
Renamed ~/Music/kgsr_broadcast_20/disc1/9|Michael Kiwanuka|I'm Getting Ready.ogg to ~/Music/kgsr_broadcast_20/disc1
9_Michael-Kiwanuka_Im-Getting-Ready.ogg
```

That's much better. Now I'm free to copy these songs wherever I want without Linux complaining.

## Conclusion

What lessons can be gleaned from my past self's poor file-naming choices? Seemingly trivial problems can provide opportunities to learn more features of a language. And, like many things with technology, at some point you're probably going to regret a choice you made in the past. But it's ok, as long as you write code to fix it, then write a blog post about your fix :laughing:.
