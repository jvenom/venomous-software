---
title: "How I studied for the AWS Certified Cloud Practitioner exam"
tags:
  - AWS
  - cloud
  - certification
  - learning
---

In this blog post I'm going to talk about how I studied for the [AWS Certified Cloud Practitioner](https://aws.amazon.com/certification/certified-cloud-practitioner/) certification exam, as well as my motivation for getting the certification and my experience at an in-person testing center on exam day.

**_tl;dr_**: I signed up for an aCloudGuru subscription and it greatly helped me focus and accelerate my learning.

**Disclaimer**  
I am not affiliated with AWS or aCloudGuru, nor do I claim ownership over any of the material linked in this blog post. Such material is the property of the respective company.
{: .notice--warning}

## Why I wanted to get the AWS Cloud Practitioner certification

I've been working with AWS in my day-to-day job since I moved to a new team in March 2022. We use various services including S3, Lambda, Step Functions, SQS, CloudWatch, and EMR. So I already had some familiarity with these services, but most of the learning I've done has been adhoc and focused on solving specific problems I was facing at work. Since my company mandates a 100% infrastructure-as-code policy for all environments except dev, most of my experience in the AWS console was viewing resources that had already been created previously using Terraform, rather than using the console to create new resources.

I wanted to get the Cloud Practitioner certification to give myself a more formal overview of all the different AWS services out there, as well as get more experience directly creating resources in the AWS console. Also, I was on paternity leave for 12 weeks, and the Cloud Practitioner certification seemed like it would be doable in that time, given my previous experience working with AWS in my day job. I plan to purse other AWS certifications in the future.

## How I did on the exam

I passed with a score of 889/1000 (the minimum passing score is 700).

## How long I studied for

I did some light self-directed studying for about a month using the [official exam guide](https://d1.awsstatic.com/training-and-certification/docs-cloud-practitioner/AWS-Certified-Cloud-Practitioner_Exam-Guide.pdf) and free courses on [aws.training](https://aws.training). Then I signed up for aCloudGuru, and started devoting around 10 hours a week to working through their courses. I did this for about 3 weeks before taking my exam.

## aCloudGuru to the rescue
 
I was struggle-bussing pretty hard when I first started with self-directed learning to prepare for this certification. I was overwhelmed by the sheer volume of material online for each topic in the exam guide. I realized I needed more carefully curated material to help direct my studying. One of my coworkers had mentioned aCloudGuru several times in the past, so I thought I'd give them a try. I signed up for an annual subscription on their Personal Plus plan. It was $370.96.

## What you get with aCloudGuru

aCloudGuru provides you with access to hundreds of courses and standalone videos. They have training for AWS, GCP, and Azure. They also have lots of general Linux content, since Linux Academy was folded into aCloudGuru, and both are now owned by Pluralsight.

One of the neat things about aCloudGuru is what they call the Cloud Playground, which is where you can spin up an AWS, GCP, or Azure sandbox environment anytime you want. Once you spin up the AWS sandbox, the environment is available for four hours, and all the resources that you create are completely free. This was a great way for me to get my hands dirty in the AWS console and play around without having to sign up for my own personal AWS account.

aCloudGuru also has discussion forums where you can help build your own knowledge by answering questions for others. I haven't really used them much yet, but I plan on using them more once I start preparing for the higher-level certifications.

## Learning paths

For me personally, the most helpful thing about aCloudGuru is its concept of "learning paths", which are structured curriculums of courses, videos, and supplementary material designed around a specific role or goal you want to achieve. For AWS they have five different ones: Architect, DevOps, Developer, Machine Learning, and Data. I chose the Data one since that's the most relevant for the type of work I do.

The learning path gives you a list of courses and supplementary material to go through in order, including prep courses for the different certification exams. There is a bit of a gamification aspect as well; as you progress through the material you complete different levels such as Novice, Apprentice, Guru, etc. As you complete each certification you can add it to your learning path to track your progress.

## Courses

Below is the list of aCloudGuru courses I took, in the order they appeared on the AWS Data learning path:

- **Introduction to AWS**: This was a good high-level overview of AWS and its services. There were some hand-ons labs as well. Some of the material is covered on the Practitioner exam, and repeated in the _AWS Certified Cloud Practitioner_ prep course.
- **AWS Certification Preparation Guide**: This has lots of great info on the structure of the exams, a bunch of really good tips for studying and practicing, and an overview of the types of questions AWS puts on the exam and the mindset you should have when working through the questions. The instructor also does a great job of explaining how to find specific AWS info and documentation, including how to Google for each thing with straightforward search terms. I plan on coming back to this course as I prepare for the other exams. The course is all videos with no labs.
- **AWS Certified Cloud Practitioner (CLF-C01)**: This is the prep course for the Certified Cloud Practitioner exam, and what I spent the bulk of my time on. The course includes several hands-on labs. Also instead of traditional slides the course material is based on interactive web pages created using Visme. Each lesson has its own page, which is linked in the Resources section for the lesson in aCloudGuru. These web pages were a great help when I was reviewing the material right before I took my exam.

I was able to watch a lot of the course videos during downtime on my phone with the aCloudGuru mobile app. The labs and quizzes only work on the web version, not on the mobile app.

**Important note**: The AWS Certified Cloud Practitioner exam is being replaced with a new version in September 2023. Make sure that the aCloudGuru prep course you take has the new exam code, CLF-C02.
{: .notice--danger}

## AWS Whitepapers

The _AWS Certified Cloud Practitioner (CLF-C01)_ course recommends that you read these two white papers in full, then review them the day before your exam. The are quite long, so you should plan for this reading time as you're going about your studying.

- [Overview of Amazon Web Services](https://docs.aws.amazon.com/pdfs/whitepapers/latest/aws-overview/aws-overview.pdf) (94 pages): Most of the white paper is dedicated to short blurbs describing each AWS service. I was strapped for time, so I skipped some of the services that I knew wouldn't be covered on the Cloud Practitioner exam, such as the IoT and Media ones.
- [How AWS Pricing Works](https://docs.aws.amazon.com/pdfs/whitepapers/latest/how-aws-pricing-works/how-aws-pricing-works.pdf) (40 pages): Most of the paper is devoted how selected AWS services are priced. You won't need to know specific numbers for the exam, just a general idea of how things are priced for each service. I ran out of time, so I skipped the "Cost calculation examples" section at the end.

One thing that was helpful for me was sending the whitepaper PDFs to my Kindle so I could read them off-and-on at random times when I didn't feel like turning on my laptop.

## Practice Exams

The _AWS Certified Cloud Practitioner (CLF-C01)_ aCloudGuru course also includes practice exams that mimic the content and format of the questions you'll see on the actual exam. Each exam is 65 questions, and you have 90 minutes to finish it, just like on the real exam. Overall I found that the questions on these practice exams were pretty similar to what I saw on the actual exam. Also, you can provide feedback on the practice exam questions, which will open up a support case with Pluralsight where someone will review your feedback or suggestions. Pretty cool. I took four different practice exams. There are six listed on the course, but I'm pretty sure you can re-take the same exam over again and it will just generate a new set of questions each time (i.e., if you take "Practice Exam 1" twice it will generate a completely different set of questions each time).

After finishing the exam, aCloudGuru shows you all the questions. For questions you answered correctly, you see some text below the correct answer explaining why that answer(s) is correct. For questions you get wrong, you see an explanation of why the answer(s) you chose was incorrect and why the correct answer is the best one. I would definitely recommend screenshotting not only the questions you got wrong but also the ones you got right but weren't sure about. This helped me when reviewing for the real exam; I could reflect deeply into why I either answered a question incorrectly or was unsure about it. This was hugely beneficial in helping me uncover gaps in my knowledge and exposing topic areas that I needed to review more.

## Exam Day

The exams are administered through Pearson through their system called Pearson VUE. You can choose to take the exam online on a personal device, or go in-person to a testing center and have the exam administered to you. I opted to take my exam at an in-person testing center at a community college near my house, because I didn't want to have to worry about installing extra software (spyware?) on my laptop and creating a work area that was compliant with AWS's and Pearson's policies. Also having a newborn baby at home is not the most conducive thing for a distraction-free environment.

On the morning of the exam, I holed up in a coffee shop near the college with my laptop. I finished reading the "How AWS Pricing Works" whitepaper, and reviewed in detail the questions I had screenshotted during my practice exams. After that I reviewed the interactive lesson webpages from the *AWS Certified Cloud Practitioner* aCloudGuru course, starting with the material from the earliest lessons since it'd been a bit since I'd seen those. I specifically did *not* want to take another practice exam this morning. The week before I had done two practice exams on the same day, and I noticed my brain was fried a little bit during the second exam attempt. If you don't have this problem, taking one last practice exam on exam day might be something you could do as well.

With about 20 minutes to go until exam time, I left the coffee shop and drove to the college (about two minutes away). I didn't bring anything into the testing center except my two forms of ID and my car keys; I left everything else in the car. I thought the testing center would be super dead since it's summer, but I was very wrong. There were tons of students there taking different exams they need in order to register for their classes, since the semester starts in a few weeks. Because of this there was a bit of a wait to sign in, and I did not go back to start my exam until about 10 minutes after my appointment time. If I take exams at this testing center in the future I will be sure to keep this in mind when scheduling my appointment. Once I got checked in everything was pretty seamless though. There were two different rooms in the testing center, one for the college exams (which is where all the students were), and one for the external exams. For pretty much the entire time I was taking my exam in the external exam room I was the only person in there, which was nice. Overall I had a great experience at this testing center, and I think I'd go back there to take the other AWS certification exams.

I finished all the exam questions with lots of time left to spare. So first I went back through all the questions I had marked as "Flag for review", then scrutinized them in more detail. For some I was able to decide on an answer, so I unflagged them and repeated the process again. Then I went through _all_ the questions again in detail to double-check my answers and to make sure that I hadn't made any mistakes, like clicking on a different answer choice from what I'd intended. Once I was satisfied I submitted my exam with about 20 minutes to spare.

Before the exam I had gotten an erasable clipboard that I could use for notes, which I turned in when I left the testing center. There were five different AWS services or concepts in either the questions or answer choices that I didn't recognize or wasn't familiar with. I wrote these on the clipboard, then memorized them before I handed the clipboard back to the test center proctor. As soon as I got to my car I added them to a Google Doc on my phone. I plan to review these things when I'm studying for my next exam.

## Conclusion

Hopefully this blog post will help you out as you're preparing for the AWS Certified Cloud Practitioner exam. Using aCloudGuru definitely helped me focus my studying and provided me with different resources to learn more effectively. I recently saw an article that their parent company, Pluralsight, is [going through some rounds of layoffs](https://www.sltrib.com/news/business/2023/07/25/its-third-round-layoffs-utah-tech/). This is such a bummer, because it really is a great platform with knowledgeable and engaging instructors. I plan to use aCloudGuru even more when studying for my other AWS certifications.
