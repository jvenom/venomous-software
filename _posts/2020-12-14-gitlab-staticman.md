---
title: Implementing Staticman Comments on a GitLab-Hosted Jekyll Blog
tags:
  - jekyll
  - staticman
  - GitLab
---

As I'm writing more blog posts, I want to give my readers a way to provide feedback directly on posts through comments. Previously I've just been sharing links to my posts on work and [SADevs](https://satx.dev) Slack channels. I've gotten some great reader interaction using that method, but I thought it was time to add a commenting system.

I chose [Staticman](https://staticman.net/) for my blog comments. In this post I'll discuss the reasoning behind my choice, describe Staticman's architecture, and detail how to implement it for a Jekyll blog that's hosted on GitLab Pages. Some of the steps should be valuable even if you aren't using Jekyll, though.

## Another Jekyll Staticman guide? Seriously? :weary:

There a plenty of other blog posts out there with detailed steps on how to implement static comments with Staticman. Pretty much all of these posts also have static comments enabled (surprise!). I'm not going to bore you with another step-by-step guide in this post. Instead I'll use [@travisdown's excellent post](https://travisdowns.github.io/blog/2020/02/05/now-with-comments.html) as a starting point, and elaborate on the bits specific to GitLab, since his post focuses on GitHub. I'd recommend keeping that post open, since I'll be referring back to it frequently.

But first, I'll explain why I chose Staticman over other alternatives.

## Why I chose Staticman

### I control my own comment data

The main reason I chose Staticman over an alternative like [Disqus](https://en.wikipedia.org/wiki/Disqus) or Facebook is that Staticman gives you control over your own blog's comment data. Since the comments are **_static_**, they are stored in the same source code repository as your blog's code, and built at the same time as your other posts and pages. By contrast, if you're using a third-party provider like Disqus or Facebook, _they_ are the ones that control your comment data. So if they block your account, or change their pricing in a way you don't like, or do any other annoying things, you could potentially lose your blog's comments.

### It's free and open-source

Staticman is a Node.js application licensed under the MIT license, so it's free for you to copy and modify if you so choose. If you do end up doing that, though, you're probably doing it wrong (more on that later).

### It's very, very cheap, if not outright free

Not only is Staticman "free as in freedom", it's ["free as in beer"](https://en.wikipedia.org/wiki/Gratis_versus_libre). If you're self-hosting the Staticman app, it's very unlikely that your blog will generate enough comment traffic that you'll need to pay for it.

### It uses standard tools

If you're using a static site framework for your blog and hosting it on GitHub or GitLab, you're already familiar with the concepts Staticman uses to create and store your comments. `git` is at the core of how staticman works, which I'll explain below.

### Other people have probably already done the hard work for you

And no, I'm not just talking about this blog post (:wink:). If you're using a Jekyll theme, there's a chance the theme maintainer already enabled Staticman support. [Mine did.](https://mmistakes.github.io/minimal-mistakes/docs/configuration/#static-based-comments-via-staticman) If that's the case you won't need to develop custom JavaScript to post your comments or modify your templates to render them. That's what I meant when I said you're "doing it wrong" if you find yourself making custom modifications to Staticman.

Now that you've seen why I chose Staticman, I'll quickly explain how it works.

## How Staticman works

[Staticman's docs](https://staticman.net/docs/) provide a brief description of how it works, and Travis's post also gives some [helpful color commentary](https://travisdowns.github.io/blog/2020/02/05/now-with-comments.html#introduction).

Staticman is a Node.js REST API that you deploy to Heroku. You have a form for a comment on each of your blog posts, and when the user submits that form, your blog code makes a `POST` to the Staticman API. Staticman will then add the comment to your GitLab project, either through a commit directly to the `master` branch, or through a merge request. Once the comment is committed/merged, it'll live inside your blog codebase as a YAML file. Your blog should have extra template code to pull in those comments and render them on the correct posts.

Here's Staticman's "architecture diagram", adapted from the doc page I linked to above.

```
Web client (your blog) <-> Staticman API (hosted on Heroku, by you) <-> GitLab project
```

Now that you've got a brief rundown of how the Staticman API works, I'll get into the specifics of connecting it with GitLab.

## Create a GitLab "bot" account

Like I mentioned earlier in this post, I'm going to skip specific steps in the process where there aren't any distinct steps for GitLab configuration. This is one of the steps where the process is different.

Your "bot" account will just be a regular user account that you'll add to your blog's GitLab project later on. You should create the account the same way you'd create any other user account. I'm using GitLab.com, but if you're self-hosting GitLab the same concepts apply. Note that you'll need a distinct email address to create the new account, one that's different from the email you use for your primary GitLab account. I didn't have one handy, so I also had to create a burner Gmail account for my bot.

## Generate an access token for your bot account

Log on to GitLab as your bot account. Open its User Settings (aka Profile).

On the left-hand menu, click "Access Tokens". You should see a page to create a new access token.

Name the access token whatever you want. Leave the expiration date blank. Select the checkboxes for the `api` and `read_user` scopes. Hit the "Create personal access token" button.

![Screenshot of GitLab personal access token configuration settings](../../assets/images/gitlab-staticman/gitlab-access-token.png)

Copy the personal access token that's displayed, and stash it in a text document or password safe. **This is the only chance you'll get to copy the access token.**

## Continue with the Staticman configuration

After you create your GitLab bot account and generate an access token for it, you can follow most of the remaining steps from Travis's post, starting [here](https://travisdowns.github.io/blog/2020/02/05/now-with-comments.html#set-up-the-blog-repository-configuration).

You need to add a `staticman.yml` file to your repo, and add some extra keys to your `_config.yml`. For these I copied the configs suggested in my theme's documentation, since the theme developer had already added staticman integration into the theme's layouts.

You'll need to configure your blog to use the `/v3` resources of the Staticman API. In the [Configuring _config.yml](https://travisdowns.github.io/blog/2020/02/05/now-with-comments.html#configuring-_configyml) section of Travis's post, he uses this URL:
```yaml
# The URL for the staticman API bridge endpoint
# You will want to modify some of the values:
#  ${github-username}: the username of the account with which you publish your blog
#  ${blog-repo}: the name of your blog repository in github
#  master: this the branch out of which your blog is published, often master or gh-pages
#  ${bridge_app_name}: the name you chose in Heroku for your bridge API
#
# for me, this line reads:
# https://staticman-travisdownsio.herokuapp.com/v2/entry/travisdowns/travisdowns.github.io/master/comments
staticman_url: https://${bridge_app_name}.herokuapp.com/v2/entry/${github-username}/${blog-repo}/master/comments
```

You'll need to change the first part of the context root to `/v3/entry/gitlab` since the `/v2` Staticman resources only support GitHub and not GitLab. The rest of the URL - `${gitlab-username}/${blog-repo}/master/comments` - is the same as in Travis's example. Here's what it looks like for my blog:
```yaml
# for me, this line reads:
staticman_url: https://jvenom-staticman.herokuapp.com/v3/entry/gitlab/jvenom/venomous-software/master/comments
# Substitute your own values for ${bridge_app_name}, ${gitlab-username}, and ${blog-repo}
# staticman_url: https://${bridge_app_name}.herokuapp.com/v3/entry/gitlab/${gitlab-username}/${blog-repo}/master/comments
```

## Set up the API bridge

Setting up the API bridge follows the same process as Travis's blog. You'll need to sign up for Heroku, download the Heroku CLI, and click the "Deploy to Heroku" button on the [staticman GitHub homepage](https://github.com/eduardoboucas/staticman).

In the step where you configure the bridge secrets, you need to add a config named `GITLAB_TOKEN` instead of `GITHUB_TOKEN`.
```
heroku config:add --app ${bridge_app_name} "GITLAB_TOKEN=${gitlab_token}"
```

Here `${gitlab-token}` is the personal access token you created [above]({% post_url 2020-12-14-gitlab-staticman %}#generate-an-access-token-for-your-bot-account).

## Skip the "Invite and Accept Bot to Blog Repo" step

You don't need to do this step since it's specific to GitHub. I spent a ridiculous amount of time trying to figure out why the `/v3/connect` endpoint on my Staticman instance wasn't working. Turns out it doesn't exist. I finally found [this GitHub PR comment](https://github.com/eduardoboucas/staticman/pull/319#issuecomment-633862869) which confirms that the `connect` call isn't required for GitLab.

Instead, you'll add your bot account as a Developer on your blog's GitLab project.

## Add the bot account to your blog's GitLab project

Login to GitLab using your main account. Navigate to your blog's project.

Click "Members" on the left-hand menu.

Invite your bot account to be a Developer on the project. Login as your bot account and accept the invitation.

## Follow the rest of the steps to post comments and render them

As I mentioned before, I skipped these steps since they were handled for me by the [Jekyll theme](https://mmistakes.github.io/minimal-mistakes/) I'm using.

## Try it out

You should now have everything in place to enable Staticman comments. You can push all your code changes to the `master` branch in your blog's GitLab project and test the comments.

If you have `comments.moderation: true` set in your `staticman.yml`, you'll get a merge request whenever someone posts a comment. Here's what that looks like:

![Screenshot of GitLab merge request generated by Staticman](../../assets/images/gitlab-staticman/staticman-merge-request.png)

When you merge the merge request, Staticman will add a YAML file to your blog's `_data` folder. This is where the comment data actually lives. You can see an example YAML file [here](https://gitlab.com/jvenom/venomous-software/-/blob/master/_data/comments/deploying-jekyll-to-gitlab-pages/comment-1607495638691.yml).

Here's what that [example]({% post_url 2020-05-15-deploying-jekyll-to-gitlab-pages %}#comment1) looks like when rendered on my blog:

![Screenshot of example comment rendered on my blog, showing a hyperlinked author name and color-formatted YAML embedded in the comment](../../assets/images/gitlab-staticman/rendered-comment.png)

The hyperlinked author name and markdown rendering were provided out-of-the-box by my Jekyll theme. Neat!

## Optional: Enable reCaptcha

I didn't have to do anything extra from Travis's blog post to [enable reCaptcha](https://travisdowns.github.io/blog/2020/02/05/now-with-comments.html#enable-recaptcha). You have to sign up for reCaptcha and add a site for your blog's domain. You'll copy the site key as-is into the config files, but you'll encrypt the secret key using the Staticman API's `/encrypt` resource. I don't think it really matters whether you use the `/v2` or `/v3` version of `/encrypt`. I used `/v3` for consistency's sake.

Note that reCaptcha is a third-party service provided by Google. It comes with its own restrictions and terms of service. I'd give those a read before enabling it on your blog.

## Conclusion

Hopefully this blog post has filled the documentation gap that exists for configuring Staticman comments with a Jekyll blog hosted on GitLab. Check out the below resources for more info:
- [Staticman docs](https://staticman.net/docs/)
- [Travis Downs' blog post](https://travisdowns.github.io/blog/2020/02/05/now-with-comments.html)
- [Staticman docs for the Minimal Mistakes Jekyll theme](https://mmistakes.github.io/minimal-mistakes/docs/configuration/#static-based-comments-via-staticman)
- [My `staticman.yml`](https://gitlab.com/jvenom/venomous-software/-/blob/master/staticman.yml)
- [The comment-related stuff in my `_config.yml`](https://gitlab.com/jvenom/venomous-software/-/blob/master/_config.yml#L33-42)

If you have some feedback that's relevant to the content of this post, you can leave a comment here. If you just want to make a "does it work?" comment, you can do that on my [test post]({% post_url 2020-12-14-staticman-testing %}).
