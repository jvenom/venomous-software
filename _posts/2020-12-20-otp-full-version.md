---
title: Using Elixir to Get the Full Erlang/OTP Version String
tags:
  - elixir
  - erlang
  - OTP
---

Lately I've been playing around more with [Elixir](https://elixir-lang.org) in my personal projects. Elixir is built on the Erlang language and the [OTP](https://erlang.org/doc/) platform. Understanding the underlying Erlang/OTP tooling is important for building competency in Elixir. In this post I'll go over how to print the full Erlang/OTP version string using Elixir.

## The problem

If you want to print the full Elixir version string, all you have to do is this:

```
$ elixir -v
Erlang/OTP 23 [erts-11.1.3] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe]

Elixir 1.11.2 (compiled with Erlang/OTP 23)
```

I wanted to find a way to print the same type of version string for an OTP installation - e.g. `23.x.x`. Let's look at some potential ways to do that.

## Possible solutions

### elixir -v

Running `elixir -v`, like in the above example, only prints out the major version. It doesn't print out the full version string.

### iex prompt

When you launch the `iex` REPL, by default the same information is printed as what's shown when you run `elixir -v`. Again, only the major version is included, not the full version with major, minor, and patch release numbers.

```
$ iex
Erlang/OTP 23 [erts-11.1.3] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe]

Interactive Elixir (1.11.2) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)>
```

### erl prompt

The `erl` prompt also only prints the OTP major version.

```
$ erl
Erlang/OTP 23 [erts-11.1.3] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe]

Eshell V11.1.3  (abort with ^G)
1>
```

### OS packages

I'm using Fedora on my development machine, so I can use [dnf](https://fedoraproject.org/wiki/DNF) to view information about installed packages. If I list all the installed Erlang packages, I can see the full version number in the version column. This solution isn't ideal, however, because it's not portable across platforms.

```bash
$ dnf list installed erlang*
Installed Packages
erlang-asn1.x86_64            23.1.4-1.fc33   @updates
erlang-compiler.x86_64        23.1.4-1.fc33   @updates
erlang-crypto.x86_64          23.1.4-1.fc33   @updates
erlang-erts.x86_64            23.1.4-1.fc33   @updates
erlang-et.x86_64              23.1.4-1.fc33   @updates
erlang-hipe.x86_64            23.1.4-1.fc33   @updates
erlang-inets.x86_64           23.1.4-1.fc33   @updates
erlang-kernel.x86_64          23.1.4-1.fc33   @updates
erlang-mnesia.x86_64          23.1.4-1.fc33   @updates
erlang-observer.x86_64        23.1.4-1.fc33   @updates
erlang-parsetools.x86_64      23.1.4-1.fc33   @updates
erlang-public_key.x86_64      23.1.4-1.fc33   @updates
erlang-runtime_tools.x86_64   23.1.4-1.fc33   @updates
erlang-sasl.x86_64            23.1.4-1.fc33   @updates
erlang-ssl.x86_64             23.1.4-1.fc33   @updates
erlang-stdlib.x86_64          23.1.4-1.fc33   @updates
erlang-syntax_tools.x86_64    23.1.4-1.fc33   @updates
erlang-tools.x86_64           23.1.4-1.fc33   @updates
erlang-wx.x86_64              23.1.4-1.fc33   @updates
```

### Erlang / Elixir functions

It's possible to retrieve the major version by calling a function in Erlang or Elixir.

In Erlang:

```
$ erl
Erlang/OTP 23 [erts-11.1.3] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe]

Eshell V11.1.3  (abort with ^G)
1> erlang:system_info(otp_release).
"23"
```

In Elixir:

```
$ iex
Erlang/OTP 23 [erts-11.1.3] [source] [64-bit] [smp:4:4] [ds:4:4:10] [async-threads:1] [hipe]

Interactive Elixir (1.11.2) - press Ctrl+C to exit (type h() ENTER for help)
iex(1)> System.otp_release()
"23"
```

Neither of these functions return the _full_ version string. However they'll still play a part in the ultimate solution, which I'll describe below.

## The solution: Reading the OTP_VERSION file

The OTP installation includes a file called `OTP_VERSION` in the OTP installation directory. The file can be found at `<otp-installation-location>/releases/<major-version>/OTP_VERSION`.

```bash
$ cd /usr/lib64/erlang/
$ cat releases/23/OTP_VERSION
23.1.4
```

This is exactly what we need, but it'd be nice to do this in a portable script that works across different OTP versions and operating systems. We can write an Elixir script that meets these criteria, using functions from the Erlang and Elixir standard libraries, and borrowing from the examples in the [Erlang docs](https://erlang.org/doc/system_principles/versions.html#retrieving-current-otp-version).

Here's what that script looks like:

```elixir
#!/usr/bin/env elixir
# otp_version.exs

defmodule OTPVersion do
  def otp_version do
    Path.join([:code.root_dir(), "releases", System.otp_release(), "OTP_VERSION"])
    |> File.read!()
    |> String.trim_trailing("\n")
  end
end

OTPVersion.otp_version() |> IO.puts()
```

We define a module called `OTPVersion` with a zero-arity function named `otp_version`. Let's get into what this function is doing.

`:code.root_dir/0` is an Erlang function that returns the path to the OTP installation. `System.otp_release/0` returns the OTP major version as a string, as we saw above. We use `Path.join/1` to join all the path components together. We pipe the joined path into `File.read!/1`, which reads the file and returns its contents (the version string). We then trim the newline character from the string.

At the bottom of the script we call the `otp_version/0` function and pipe the return value to `IO.puts/1`. Why am I stripping the newline from the string in the `otp_version/0` function if `IO.puts()` will just add it back? I wanted to make sure that the function only returns the string itself without any additional characters. This will make it easier in the future if I decide to refactor `otp_version` into a normal Elixir library function instead of a script.

Let's see the script in action:

```bash
$ ./otp_version.exs 
23.1.4
```

## Conclusion

In this post I demonstrated how to use Elixir to write a portable script that prints the full Erlang/OTP version to standard out. I could've written the script in Erlang instead, but I thought I'd use it as a learning exercise to learn more about parity between functions in the Erlang and Elixir standard libraries.

I'm planning on writing a few more blog posts about Elixir development tooling over the next few days. Stay tuned!
