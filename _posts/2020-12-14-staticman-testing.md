---
title: Test Post for Staticman Comments
tags:
  - jekyll
  - staticman
---

This is a post for you to test leaving comments on my blog (courtesy of [staticman](https://staticman.net/)). When you submit a comment, I'll get a merge request. If I approve and merge the merge request, a CI pipeline will be triggered. Your comment will appear once that pipeline finishes. If I reject the merge request, your comment won't ever appear.

For all of my posts, I reserve the right to reject your comment for any reason whatsoever.
