---
title: Deploying a Jekyll Site to GitLab Pages with GitLab CI
tags:
  - jekyll
  - GitLab
  - ci
---

When I chose Jekyll as the static-site framework for this blog, I thought about hosting it on GitHub using [GitHub Pages](https://pages.github.com/), since GitHub provides a [github-pages](https://github.com/github/pages-gem) Ruby gem that includes Jekyll and some select plugins. But then I decided to set up a GitLab.com account and host it with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). We use an on-prem GitLab instance at work, so I thought hosting this blog on GitLab Pages would help me get some more practice writing GitLab CI jobs. It's not quite [eating your own dog food](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) - more like using the same brand of dog food at home as you do at work. Maybe that metaphor makes more sense to you if you work at one of those cool startups where you can bring your dog to work. But I digress.

In this blog post I'll show how I went about deploying my Jekyll site, and I'll highlight some of things I learned along the way. For the most part I'll be comparing my workflow to [this page from the GitLab docs](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html) since I used it as starting point. I'll show how I deviated from that example and explain why I chose to do so. Before I do that, I'll go over some prerequisites that need to be configured in your Jekyll project.

## Prerequisites
### Use the Jekyll gem
Don't use the `github-pages` Ruby gem. Install Jekyll like you normally would if you weren't deploying to GitHub Pages. If you're using a theme, make sure you also install it as a gem - in other words don't use `remote_theme` in your `_config.yml`. This blog uses the [Minimal Mistakes](https://mademistakes.com/work/minimal-mistakes-jekyll-theme/) theme. Here's an example Gemfile:

```ruby
source "https://rubygems.org"

gem "jekyll", "~> 4.0.0"
gem "minimal-mistakes-jekyll"
# Install your other other dependencies here...

group :jekyll_plugins do
  gem "jekyll-include-cache" # required by Minimal Mistakes
  # Install your other plugins here...
end
```

### Set the baseurl key in your _config.yml
**Warning:** Forgetting this step might cause your site to build incorrectly!
{: .notice--warning}
The first time I built my site and deployed the `master` branch to GitLab Pages, I opened my blog's [About](https://jvenom.gitlab.io/venomous-software/about/) and discovered that all of the CSS was missing. I had forgotten to set the `_baseurl` key in my project's `_config.yml`. You need to set this key to match the name of your GitLab Project. Your site will be deployed to `https://<yourname>.gitlab.io/<project-name>`. My `<project-name>` is `venomous-software`, and my blog on GitLab Pages is `https://jvenom.gitlab.io/venomous-software`. So in my blog's `_config.yml` I set `baseurl` like so:

```yaml
baseurl: "/venomous-software"
```
You don't need to worry about setting the `url` key, only `baseurl`.

Now that we've got these prereqs out of the way, we can take a look at an example GitLab CI configuration and review some of its core concepts.

## Examine the example .gitlab-ci.yml

You use the `.gitlab-ci.yml` file to define the actions GitLab CI takes for your project (where a "project" in this case is what GitHub calls a "repository"). Put the file at the root of your project, and then GitLab will execute a CI process using whatever jobs you define in it. There are way too many possible configurations to review here, so I'd recommend giving the [README](https://docs.gitlab.com/ee/ci/yaml/README.html) a good once-over. Let's take a look at our [example GitLab pages YAML](https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html) to see the most important bits.

```yaml
image: ruby:2.6

pages:
  stage: deploy
  script:
    - bundle install
    - bundle exec jekyll build -d public
  artifacts:
    paths:
      - public
  only:
    - master

test:
  stage: test
  script:
    - bundle install
    - bundle exec jekyll build -d test
  artifacts:
    paths:
      - test
  except:
    - master
```

Here we see an `image` key at the top of the file, followed by two different blocks that represent our jobs. The `image` key specifies the Docker image you want the GitLab runner to use to run your job. The jobs are assigned to stages which execute in the order you define them. All jobs in a stage execute in parallel. By chaining your stages and their jobs together, you can execute your full CI workflow (what GitLab calls a "pipeline").

Both jobs are basically doing the same thing. Let's look a the `pages` job first, which is what actually deploys your Jekyll site to GitLab pages. The `script` list in your jobs contains the meat of your workflow. You can call whatever scripts you want here, and they can be provided by the Docker image or sourced locally from your project. Scripts will be executed relative to the root of your project. In our case we do the usual `bundle install`, then run `jekyll build`. The only thing out of the ordinary is that we tell Jekyll to build our site in the `public` folder instead of the default `_site`. The `public` folder will be created at the root of the project. 

We also create an `artifacts` definition to tell GitLab CI to persist this `public` folder so it can be deployed to the GitLab Pages site. If you need to share files or data between jobs in the same pipeline, artifacts a good way to do that. One example I've seen is having a `test` job that writes an HTML test report or coverage report as an artifact, then having a subsequent job that checks the file or uploads it to another location. This is one possible way you could implement quality gates in your pipeline.

The `only` key means that this job will only be run for pipelines on the `master` branch. You can specify a wide variety of regex-based ref definitions in `only` and it's counterpart, `except`. Check out the README page for more info.

The `test` job is next. It's very similar to `pages`, except it builds the Jekyll site in a folder called `test` instead of `public` (and saves that folder as an artifact). It will run for every feature branch or tag except for the `master` branch. These pipelines will _not_ deploy anything to GitLab Pages. We just have this job so we can make sure we didn't break the Jekyll build when we push to our feature branches, and so we can examine the generated HTML artifacts and make sure they look good before they go live on GitLab Pages.

## Update the Ruby version

The first thing we'll do is to update the version of Ruby we're using to run our jobs. When I wrote this post the latest version was 2.7, and you'll probably have a later one available, too. So I changed my `image` key to the following:

```yaml
image: ruby:2.7
```

## Improve readability

There are some things we can do to improve the file's readability and ensure that any future readers have a clear idea of what our pipeline is doing. It's always good practice to explicitly define your jobs' stages with a `stages:` key, and to order them in the order that your stages run. This isn't such a big deal for our simple configuration with only two jobs and stages, but it's extremely helpful for large, complex pipelines with many stages. Let's do that now. Here's the updated YAML:

```yaml
image: ruby:2.7

stages:
  - test
  - deploy

test-pages-build:
  stage: test
  script:
    - bundle install
    - bundle exec jekyll build -d test
  artifacts:
    paths:
      - test
  except:
    - master

pages:
  stage: deploy
  script:
    - bundle install
    - bundle exec jekyll build -d public
  artifacts:
    paths:
      - public
  only:
    - master
```

It's looking better already. I opted to rename the `test` job to `test-pages-build` to better reflect what it's doing. Then I put it before the `pages` job, and added a `stages` key to explicitly define the `stages`. If you don't do this GitLab CI will allow you to use the `build`, `test`, and `deploy` stages by default. It's better to be explicit, though. Let's continue on with our enhancements.

## Use the default key for global configurations

Our `image` definition at the top of the file is shared by both jobs. We could define it on a per-job basis if we wanted to. GitLab CI has other keys that you can define on a global or per-job basis. Looking over the .gitlab-ci.yml README, I noticed [this](https://docs.gitlab.com/ee/ci/yaml/README.html#globally-defined-image-services-cache-before_script-after_script):
> Defining `image`, `services`, `cache`, `before_script`, and `after_script` globally is deprecated. Support could be removed from a future release.  
> Use `default:` instead.

Let's follow this advice and add a `default` key and move `image` under it.
```yaml
default:
  image: ruby:2.7
```

## Use before_script for code reuse

There's some redundancy in our current job definitions. In both jobs we're running `bundle install`. We can move this to a separate `before_script` key under `default`, and it will be reused by both jobs, similarly to how `image` is. Here's the updated YAML:

```yaml
default:
  image: ruby:2.7
  before_script:
    - bundle install

stages:
  - test
  - deploy

test-pages-build:
  stage: test
  script:
    - bundle exec jekyll build -d test
  artifacts:
    paths:
      - test
  except:
    - master

pages:
  stage: deploy
  script:
    - bundle exec jekyll build -d public
  artifacts:
    paths:
      - public
  only:
    - master
```

We added `bundle install` as a `before_script`. It will be called before the main `script` step in both jobs. We then removed it from the individual job definitions. GitLab CI also supports other mechanisms for code reuse, like YAML anchors and inclusion of external YAML files. I'll leave those as an exercise for the reader.

## Speed up your pipelines

This is where I was at when I first deployed my blog to GitLab Pages. It worked great, but I noticed that my pipelines were taking a long time to run - around 5 minutes or more. The `bundle install` step was taking the vast majority of the time. The next sections will show you how to optimize the pipeline by reducing the time spent on gem installation.

### Use parallel jobs and caching

GitLab has a [caching mechanism](https://docs.gitlab.com/ee/ci/caching/index.html) that allows your pipeline to persist data by storing it as a zip file on the runner or in the cloud (depending on which version of GitLab you're using and how the runner is configured). We can use this cache to save our gems between job runs so `bundle install` doesn't take as much time. In fact the above page has a [specific section](https://docs.gitlab.com/ee/ci/caching/index.html#cache-ruby-dependencies) on how to do this for Ruby jobs. Let's make those changes.

```yaml
default:
  image: ruby:2.7
  cache:
    paths:
      - vendor/ruby
  before_script:
    - bundle install -j $(nproc) --path vendor/ruby

stages:
  - test
  - deploy

test-pages-build:
  stage: test
  script:
    - bundle exec jekyll build -d test
  artifacts:
    paths:
      - test
  except:
    - master

pages:
  stage: deploy
  script:
    - bundle exec jekyll build -d public
  artifacts:
    paths:
      - public
  only:
    - master
```

We added a `cache` block to tell GitLab CI to use the path `vendor/ruby` as our cache. This path will be created in the Docker container running our jobs, relative to the root of the project. We also add the `--path` switch to `bundle install` to tell bundler to use `vendor/ruby` as the gem path. Then we add the `-j` switch so the command will run in parallel, using the maximum number of processors available.

Here's the `test-pages-build` job log from when I pushed these changes to a feature branch. The log has been truncated to only show the interesting parts.

```
Restoring cache
00:01
Checking cache for default...
  FATAL: file does not exist                         
Failed to extract cache
Downloading artifacts
00:02
Running before_script and script
04:08
$ bundle install -j $(nproc) --path vendor/ruby
[DEPRECATED] The `--path` flag is deprecated because it relies 
on being remembered across bundler invocations, which bundler 
will no longer do in future versions. Instead please use
`bundle config set path 'vendor/ruby'`, and stop using this flag
Fetching gem metadata from https://rubygems.org/..........
Fetching gem metadata from https://rubygems.org/.
Resolving dependencies...
Fetching multi_json 1.14.1
Installing multi_json 1.14.1
...Other gems get downloaded and installed...
...some time later
 $ bundle exec jekyll build -d test
...some Jekyll build output here...
Running after_script
00:01
Saving cache
00:10
Creating cache default...
vendor/ruby: found 5487 matching files             
Uploading cache.zip to https://storage.googleapis.com/gitlab-com-runners-cache/project/18765994/default 
Created cache
```

This job took about as long to run as previous jobs without caching. I didn't really see an improvement from adding the `-j` flag like I thought I would, but that's ok.

We can see that the job failed extracting the cache, but that's expected since the cache doesn't exist yet. By default GitLab CI uses a "pull-push" cache policy where it will try to "pull" the cache at the beginning of the job, and "push" it at the end. You can also specify a key to use for your cache, which allows you to use separate caches for different branches. If you don't specify a key, GitLab CI will use `default`. At the end of job you can see that GitLab CI successfully created our cache as a zip file and uploaded it to Google Cloud.

### Remove the deprecated --path flag

The last thing we'll fix is this deprecation warning that bundler printed out:
```
[DEPRECATED] The `--path` flag is deprecated because it relies 
on being remembered across bundler invocations, which bundler 
will no longer do in future versions. Instead please use 
`bundle config set path 'vendor/ruby'`, and stop using 
this flag
```

It's a pretty specific warning, with equally specific instructions on how to fix it. We can do this in our `.gitlab-ci.yml` by adding another `before_script` before `bundle_install`:
```yaml
default:
  image: ruby:2.7
  cache:
    paths:
      - vendor/ruby
  before_script:
    - bundle config set path 'vendor/ruby'
    - bundle install -j $(nproc)
# Rest of .gitlab-ci.yml omitted for brevity
```

When we push the changes, our job will use the cache we created previously, and we should see the deprecation warning go away. Here's what the output looks like:
```
Restoring cache
00:07
Checking cache for default...
Downloading cache.zip from https://storage.googleapis.com/gitlab-com-runners-cache/project/18765994/default 
Successfully extracted cache
Downloading artifacts
00:02
Running before_script and script
00:07
$ bundle config set path 'vendor/ruby'
$ bundle install -j $(nproc)
Fetching gem metadata from https://rubygems.org/..........
Fetching gem metadata from https://rubygems.org/.
Resolving dependencies...
Using multi_json 1.14.1
Using activesupport 3.1.12
Using public_suffix 4.0.5
```

Notice the output from `bundle install` - we see `Using <gem>` instead of `Fetching <gem>` and `Installing <gem>`. GitLab CI is using the gems we cached earlier instead of installing them again. My pipelines are now taking about a minute and ten seconds, instead of five minutes or more. That's a great performance improvement!

## Conclusion

GitLab CI can be a powerful tool to use for your deployment pipelines. Hopefully you've also seen that some of the same strategies you use for developing software - modifying someone else's code to suit your needs, iterating over it, refactoring it, making it modular, and optimizing it - can also be used as you develop CI/CD pipelines. Something seemingly simple like a static blog site can be a great way to help you explore the CI framework's rich functionality.
