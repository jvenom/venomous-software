---
title: "Suburban Hellscape: The Terrifying Dystopian Neighborhood in _Christmas with the Kranks_"
tags:
  - movies
  - Christmas
---

There's nothing I love more during the holidays than criticizing things people hold dear, like classic holiday movies. One such movie is _Christmas with the Kranks_, which I recently re-watched while digesting a Thanksgiving feast at my in-laws'.

In this classic Christmas comedy, suburban Chicago homeowners Luther and Nora Krank decide to skip out on Christmas to go on a cruise, only to reverse their plans at the last minute once they learn that their daughter, Blair, is planning on coming home for Christmas with her new boyfriend, Enrique. The neighborhood pitches in to throw Blair a grand soiree, and everyone is mightily content with themselves for being filled with the Christmas spirit, or whatever.[^1]

My intent in writing this blog post is not to criticize any of the technical aspects of the film or its acting. The point is this: the Kranks' neighborhood epitomizes the worst aspects of suburbia.

## Luther and Nora's neighbors are nosy jerks

Once the Kranks decide they're "skipping" Christmas by going on a cruise and not throwing their usual Christmas Eve party, everyone in their neighborhood flips out. This flipping out is equally parts selfish "I'm going to miss out on a really good party! Bummer for me :disappointed:", and authoritarian "You refuse to conform to neighborhood expectations! Shame on you!". The Kranks are harassed in turn by various neighbors, carolers, and local children. When approaching the Kranks' unlit house, the carolers ask "Are they Jewish? No? Buddhist, or anything?" As if being non-Christian is the only acceptable excuse for skipping Christmas, not "because I damn well feel like it".

The biggest jerk of them all is Vic Frohmeyer.

## Vic Frohmeyer is a dictator

Nora describes Vic Frohmeyer as the "unofficial ward boss of the neighborhood". Does that sound like a neighborhood you want to live in? Me neither. This is suburban Riverside in 2004, not inner-city Chicago in 1895. If your grip over a neighborhood is described in these terms, you're a dictator.

Frohmeyer's outsized importance can be illustrated by the scene when Blair finally arrives at her parents' house against a backdrop of snow-covered suburban Christmas idyll. He's the first person to greet Blair and Enrique as they're stepping out of the car - not Luther and Nora, who are looking on from an upstairs window. This is insane.

## Vic Frohmeyers exist in the world - in homeowners' associations

Frohmeyer's power might be exaggerated in the movie, but all around the country characters like him exist in homeowners' associations, which themselves have been vested with large amounts of [quasi-governmental authority](https://www.jstor.org/stable/23017460?seq=1). The kind of person that wants to be in charge of an HOA is not the kind of person I want to hang out with. What benefits is the HOA actually providing you? Making the neighborhood safer by telling you that you can't build a shed in your backyard? At least in _Christmas with the Kranks_, Frohmeyer's power in the HOA stands in stark contrast to the massive incompetence displayed by actual municipal services, like the police.

## There's massive waste and incompetence of municipal resources

Is your HOA powerful enough to order the police to go pick up your neighbors' daughter from the airport? That's exactly what happens in the movie. The cops not only waste taxpayer dollars by giving what's essentially a free taxi ride to a civilian, they also break protocol by nabbing a perp and **leaving him unattended in the police cruiser while they partake in the party**. I don't want to live somewhere where the HOA tells cops what to do, and where the cops think going to a Christmas party is more important than taking a criminal to jail.

## Conclusion

Don't let this supposedly light-hearted holiday feel-good movie fool you. There's something much more sinister at play in the fictional neighborhood dynamics of Riverside, Illinois. If your neighbors or HOA start acting like Nora and Luther Kranks', you need to move out as fast as you can.

[^1]: My wife astutely pointed out that this whole debacle could've been avoided if Nora and Luther had just told Blair that they were going a cruise.
